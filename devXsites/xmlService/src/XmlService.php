<?php
namespace devXsites\XmlService;

use devXsites\XmlService\XmlReader;
use devXsites\XmlService\XmlStructure;

/*
	*This class is the gate for the Xml parsing. It will bw called outside of the package and 
	*instantiate the required classes/functions 
**/
class XmlService {

	/**
	* This function recieives the xml url/path amd the shopID instantiate XmlReader instance
	*and call to the read function
	* @param  $destinationPath the xml url/path
	* @param  $shopID the specific shop id
	* @return the result collection coming fron the XmlReader
	*/

	public function parseXml($destinationPath,$shopID) {
		$xmlReader = new XmlReader(new XmlStructure($shopID));
		return $xmlReader->read($destinationPath);
	}                                  
	
}
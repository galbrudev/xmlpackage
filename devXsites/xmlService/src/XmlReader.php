<?php
namespace devXsites\XmlService;
use Sabre\Xml\Service;
use Sabre\Xml\Reader;
use devXsites\XmlService\ProductConfig;
use Illuminate\Support\Collection;
use Exception;
/**
 * This class is reponsible for parding the xml file return and fullfuill the collection 
 * result 
 * with the neccessery data.
*/

class XmlReader {

	private $xmlStructure;

	/**
	* The construcor recieves the xmlStructure injection outside the scope from the XmlService
	* class, thus make the modification of the xml more easy because it can recieves any type 
	* of xmlStructure class in the future.
	* @param $xmlStructure - The xmlStructure class instance
	*/	
	*/
	public function __construct($xmlStructure) {
		$this->xmlStructure = $xmlStructure;
	}

	/**
	* This is the main function which recieves the xml url/path. First it makes validation for 
	* the validity of the file src and for the exsitence of the ChainID(StoreID). 
	* 
	* @param $destinationPath - The xml url/path
	* @return Flase if validation else returns xml result collection
	*/

	public function read($destinationPath) {
		$toRet = TRUE;
		$service = new Service();
		$productConfig = ProductConfig::getProductConfig();

		if(! $this->checkIfChainIDExist($ChainID,$productConfig) || 
		($xml = $this->fetchXmlFromDestinationPath($destinationPath)) === FALSE) {
			$toRet = FALSE;
		}

		if($toRet !== FALSE) {
			$service->elementMap = $this->setService($service,$productConfig);
			$toRet = $this->xmlToCollection($service->parse($xml));
		}
		
		return $toRet;
	}

	/**
		* This function serve the read function by instansiate the elemenMap for the Sabre/Xml 
		* service. the elementmap is actually map the data into collection where the prices 
		* attributes is being fetched from the ProductConfig array and then mspped as array of 
		* the "Prices" or the "Items" array
		* @param $service - The Sabre/Xml service instance
		* @param $productConfig - The productConfig array
		* @return The service elementmap array
	*/
	private function setService($service,$productConfig) {
		return  [ 
			$productConfig[$this->xmlStructure->ChainID]['childProductAttribute'] 
			=>function(Reader $reader) {
       			 return \Sabre\Xml\Deserializer\keyValue($reader, '');
    		},
			 $productConfig[$this->xmlStructure->ChainID]['parentProductAttribute']
			 => function(Reader $reader) use($productConfig) {
					return \Sabre\Xml\Deserializer\repeatingElements(
						$reader, 
						$productConfig[$this->xmlStructure->ChainID]['childProductAttribute']
					);
			},
			
			$productConfig[$this->xmlStructure->ChainID]['rootAttribute']  
			=> function(Reader $reader) {
       			 return \Sabre\Xml\Deserializer\keyValue($reader, '');
    		},
		];
	}

	/**
	 * This function recieves the xml url/path and returns the content of it to the service 
	 * parser.
	 * @param $destinationPath - the xml url/path
	 * @return The xml file content, if the file path is no valid the function will return false
	 */ 

	*/	
	private function fetchXmlFromDestinationPath($destinationPath) {
		return @file_get_contents($destinationPath);	
	}

	/**
	 * This funciton relies on the XmlStructure class and checks whether the array which came 
	 * from the service parser possess all the XmlStructure attributes. Afterward it will 
	 * assign the instance properties with the calue came from the xml collection and convert 
	 * the instance into collection able to produce actions like add,remove,search etc.
	 * @param $xml - The xml parse file
	 * @return The XmlStructure converted into collection
	 */
	*/
	private function xmlToCollection($xml) {
		foreach($xml as $attribute => $value) {
			if(isset($this->xmlStructure->$attribute)) {
				$this->xmlStructure->$attribute = $value;
			}
		}
		return collect($this->xmlStructure);
	}
}
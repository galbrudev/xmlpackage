<?php

namespace devXsites\XmlService;

/**
	This class depict the how the data model should looks like.
*/
class XmlStructure {

	public $ChainID;
	public $StoreID = '';
	public $Products = [];

	public function __construct($ChainID) {
		$this->ChainID = $ChainID;
	}
}
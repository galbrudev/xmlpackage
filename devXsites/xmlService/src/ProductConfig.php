<?php

namespace devXsites\XmlService;

/*
* This class reponsible to store the settings of each shop's xml attributes.
* For instance the "supresal" has different prices attribute fom the "victory" xml
* and therfore it serves the XmlReader class to distinguish between the difference of the  xml
* structures
**/
class ProductConfig {
	public static function getProductConfig() {
		return [
			'7290027600007' => [
				'rootAttribute' => 'root',
				'parentProductAttribute' => 'Items',
				'childProductAttribute' => 'Item',

			],
			'7290696200003' => [
				'rootAttribute' => 'Prices',
				'parentProductAttribute' => 'Products',
				'childProductAttribute' => 'Product',
			],
		];
	}
}
	

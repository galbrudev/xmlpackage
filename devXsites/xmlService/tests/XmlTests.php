<?php

namespace packages\devXsites\xmlService\tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use devXsites\XmlService\XmlService;
use devXsites\XmlService\ProductConfig;

class XmlTests extends TestCase
{
    /**
     * This test checks the three elementary condition of the xml parsing proccess.
     * if the first "if" condition is occur that's mean the xml url/file is correct and valid
     * and the ChainID(ShopID) id valid and exist in the productConfig array.
     * Afterward the test continue to assertContains functions to test whether the result array
     * contains the required keys for our coollection coming back fron XmlReader class after 
     * parsing the xml file. If the first condition not occuring the assertion will be false.
     *
     * @return void
     */
    public function testParsingXmlValidation() {
    	$destinationPath = __DIR__ . '\testFiles\Price7290696200003-003-201702011707-001.xml';
    	$ChainID = '7290696200003';

    	$parsedXml = (new XmlService())->parseXml($destinationPath,$ChainID);
    	if($parsedXml !== FALSE) {
    	 	$this->assertTrue(true); 
    	 	$keys = $parsedXml->keys();
	    	$this->assertContains('ChainID',$keys);
	    	$this->assertContains('StoreID',$keys);
	    	$productConfig = ProductConfig::getProductConfig();
	    	$this->assertContains(
	    		$productConfig[$ChainID]['parentProductAttribute'],$keys
	    	);
	    } else {
	    	$this->assertFalse(false);
	    }
}
